{
    'name': 'Warung Pintar Test [Custom Points] on contact',
    'category': 'Hidden',
    'description': """
    Add Fields points on contact, and pop up in points of sales
=====================================================
        """,

    'author': 'Faris Bassam',
    'website': 'http://wopopwopop.wordpress.com',
    'version': '1.0',
    'depends': ['base'],
    'data': [
        'views/res_partner_view.xml',
        'views/point_of_sales.xml',
        # 'src/xml/pos.xml',
    ],
    'demo': [],
    'qweb': [
        'static/src/xml/pos.xml',
    ],

    'auto_install': False,
}
